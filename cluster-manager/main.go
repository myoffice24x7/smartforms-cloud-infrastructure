package main

import (
	"crypto/tls"
	"encoding/json"
	"errors"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"sort"
	"strings"
	"time"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	awss3 "github.com/aws/aws-sdk-go/service/s3"
	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"github.com/goware/cors"
	"golang.org/x/crypto/acme/autocert"
)

var config struct {
	Hostname           string
	ListenAddrInsecure string
	DynamoAccessKey    string
	DynamoSecretKey    string
	BitbucketUser      string
	BitbucketSecret    string
	ApiKey             string
}

var (
	ddb *dynamodb.DynamoDB
	s3  *awss3.S3
)

type objectsByDate []*awss3.Object

func (s objectsByDate) Len() int { return len(s) }
func (s objectsByDate) Swap(i, j int) {
	s[i], s[j] = s[j], s[i]
}
func (s objectsByDate) Less(i, j int) bool {
	return s[i].LastModified.Before(*(s[j].LastModified))
}

func main() {
	flag.StringVar(&config.Hostname, "hostname", os.Getenv("MANAGER_HOSTNAME"), "")
	flag.StringVar(&config.ListenAddrInsecure, "listen-addr-insecure", os.Getenv("MANAGER_LISTEN_INSECURE"), ":80")
	flag.StringVar(&config.DynamoAccessKey, "dynamo-access-key", os.Getenv("MANAGER_DYNAMO_ACCESS_KEY"), "")
	flag.StringVar(&config.DynamoSecretKey, "dynamo-secret-key", os.Getenv("MANAGER_DYNAMO_SECRET_KEY"), "")
	flag.StringVar(&config.BitbucketUser, "bb-user", os.Getenv("MANAGER_BB_USER"), "")
	flag.StringVar(&config.BitbucketSecret, "bb-secret", os.Getenv("MANAGER_BB_SECRET"), "")
	flag.StringVar(&config.ApiKey, "api-key", os.Getenv("MANAGER_API_KEY"), "")
	flag.Parse()

	awsCfg := (&aws.Config{}).
		WithRegion("ap-southeast-2").
		WithCredentials(credentials.NewStaticCredentials(config.DynamoAccessKey, config.DynamoSecretKey, ""))

	sess := session.New(awsCfg)
	ddb = dynamodb.New(sess)
	s3 = awss3.New(sess)

	mux := chi.NewRouter()
	mux.Use(middleware.Recoverer)
	mux.Use(cors.New(cors.Options{
		AllowedOrigins:   []string{"*"},
		AllowedMethods:   []string{"GET", "POST", "OPTIONS"},
		AllowedHeaders:   []string{"Authorization", "Accept", "Content-Type"},
		AllowCredentials: true,
	}).Handler)

	mux.Route("/api/v1", func(r chi.Router) {
		r.Use(authenticatedOnly)
		r.Get("/tags", serveGetTags)
		r.Get("/clusters", serveGetClusters)
		r.Post("/cluster/{name}", serveUpdateCluster)
	})

	mux.Get("/status/{name}", serveStatus)

	mux.Get("/", serveRedirectToUi)

	insecureMux := chi.NewRouter()
	insecureMux.Get("/", serveRedirectToUi)

	if config.Hostname != "" {
		// Let's Encrypt manager
		certManager := &autocert.Manager{
			Prompt: func(tos string) bool {
				return true
			},
			Email:      "ops@fifthocean.com.au",
			HostPolicy: autocert.HostWhitelist(config.Hostname),
		}

		// HTTPS (primary) listener
		go func() {
			srv := &http.Server{
				Addr:      ":https",
				TLSConfig: &tls.Config{GetCertificate: certManager.GetCertificate},
				Handler:   mux,
			}
			log.Println(srv.ListenAndServeTLS("", ""))
		}()

		// HTTP (insecure) listener, for redirect and http-01 validation
		if err := http.ListenAndServe(config.ListenAddrInsecure, certManager.HTTPHandler(insecureMux)); err != nil {
			log.Println(err)
		}
	} else {
		if err := http.ListenAndServe(config.ListenAddrInsecure, mux); err != nil {
			log.Println(err)
		}
	}
}

func authenticatedOnly(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		auth := r.Header.Get("Authorization")
		if auth != config.ApiKey {
			http.Error(w, http.StatusText(http.StatusUnauthorized), http.StatusUnauthorized)
			return
		}
		next.ServeHTTP(w, r)
	})
}

func serveGetClusters(w http.ResponseWriter, r *http.Request) {
	params := (&dynamodb.ScanInput{}).SetTableName("smartforms-clusters")
	res, err := ddb.Scan(params)

	out := map[string]interface{}{}

	if err != nil {
		log.Println(err)
		serveError(w, errors.New("Failed to get cluster data"))
		return
	}

	for _, item := range res.Items {
		unwrapped := map[string]interface{}{}
		wrapped := *(item["value"].S)
		if err := json.Unmarshal([]byte(wrapped), &unwrapped); err != nil {
			log.Println(err)
			continue
		}
		out[(*(item["key"].S))[1:]] = unwrapped
	}

	serveJson(w, out)
}

func serveUpdateCluster(w http.ResponseWriter, r *http.Request) {
	name := chi.URLParam(r, "name")
	if name == "" {
		http.Error(w, "No name", http.StatusBadRequest)
		return
	}

	buf, err := ioutil.ReadAll(r.Body)
	if err != nil {
		serveError(w, err)
		return
	}

	defer r.Body.Close()

	input := &dynamodb.UpdateItemInput{
		TableName: aws.String("smartforms-clusters"),
		Key: map[string]*dynamodb.AttributeValue{
			"key": {S: aws.String("/" + name)},
		},
		AttributeUpdates: map[string]*dynamodb.AttributeValueUpdate{
			"value": &dynamodb.AttributeValueUpdate{
				Value: &dynamodb.AttributeValue{
					S: aws.String(string(buf)),
				},
			},
		},
	}

	if _, err := ddb.UpdateItem(input); err != nil {
		serveError(w, fmt.Errorf("Failed to update cluster: %v", err))
		return
	}
}

func serveGetTags(w http.ResponseWriter, r *http.Request) {
	resp, err := s3.ListObjectsV2(&awss3.ListObjectsV2Input{
		Bucket: aws.String("smartforms-deploy"),
		Prefix: aws.String("smartforms-release-v."),
	})
	if err != nil {
		log.Println(err)
		serveError(w, errors.New("Unable to fetch tags"))
		return
	}

	objs := objectsByDate(resp.Contents)
	sort.Sort(objs)
	tags := []string{}
	for _, object := range objs {
		name := *object.Key
		name = strings.TrimPrefix(name, "smartforms-release-")
		name = strings.TrimSuffix(name, ".zip")
		tags = append(tags, name)
	}

	serveJson(w, tags)
}

type statusEntry struct {
	value   bool
	created int64
}

var statusCache map[string]statusEntry

func serveStatus(w http.ResponseWriter, r *http.Request) {
	if statusCache == nil {
		statusCache = map[string]statusEntry{}
	}

	name := chi.URLParam(r, "name")
	if name == "" {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	if v, exists := statusCache[name]; exists && time.Since(time.Unix(v.created, 0)) < (30*time.Second) {
		w.Header().Set("X-Status-Age", time.Since(time.Unix(v.created, 0)).String())
		json.NewEncoder(w).Encode(map[string]interface{}{
			"is_maintenance": v.value,
		})
		return
	}

	req := dynamodb.QueryInput{
		TableName: aws.String("smartforms-clusters"),
		KeyConditions: map[string]*dynamodb.Condition{
			"key": {
				ComparisonOperator: aws.String("EQ"),
				AttributeValueList: []*dynamodb.AttributeValue{
					{
						S: aws.String("/" + name),
					},
				},
			},
		},
	}

	resp, err := ddb.Query(&req)
	if err != nil {
		log.Println("error fetching status", name, err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	var out struct {
		IsMaintenance bool `json:"maintenance_mode_enabled"`
	}

	if len(resp.Items) == 0 {
		w.WriteHeader(http.StatusNotFound)
		return
	}

	if err := json.Unmarshal([]byte(*(resp.Items[0]["value"].S)), &out); err != nil {
		log.Println("error unmarshaling response", name, err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	statusCache[name] = statusEntry{
		value:   out.IsMaintenance,
		created: time.Now().Unix(),
	}

	w.Header().Set("X-Status-Age", "0")
	json.NewEncoder(w).Encode(map[string]interface{}{
		"is_maintenance": out.IsMaintenance,
	})

}

func serveJson(w http.ResponseWriter, v interface{}) {
	buf, err := json.Marshal(v)
	if err != nil {
		log.Println("error marshaling response", v, err)
		http.Error(w, "Internal error", http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	fmt.Fprint(w, string(buf))
}

func serveError(w http.ResponseWriter, err error) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusInternalServerError)

	buf, _ := json.Marshal(map[string]interface{}{
		"error": err.Error(),
	})
	fmt.Fprint(w, string(buf))
}

func serveRedirectToUi(w http.ResponseWriter, r *http.Request) {
	http.Redirect(w, r, "https://ui.manager.smartfor.ms", 302)
}

#!/usr/bin/env bash
haproxy -f /etc/haproxy.cfg -db 2>&1 &
sleep 10
echo -e "127.0.0.2 maven.ci.myoffice24x7.com\n127.0.0.3 maven.repository.redhat.com\n127.0.0.4 repo.maven.apache.org" >> /etc/hosts 
NS=$(grep "nameserver " /etc/resolv.conf | head -n 1 | awk '{print $2}')
sed -i "s/8.8.8.8/${NS}/" /etc/haproxy.cfg

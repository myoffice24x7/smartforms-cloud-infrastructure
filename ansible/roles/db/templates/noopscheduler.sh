#!/usr/bin/env bash
DEVS=$(ls /sys/block/ | egrep '^([shv]|xv])d[a-z]$')
for DEV in "$DEVS"; do
  echo noop > "/sys/block/${DEV}/queue/scheduler"
done
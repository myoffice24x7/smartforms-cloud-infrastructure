#!/usr/bin/env bash

set -eu -o pipefail

# Check the downloads we need exist
if [ ! -e /root/downloads/wildfly.zip ]
then
	echo "Wildfly download is missing from /root/downloads/"
	exit 1
fi

DEPLOY_INFO=$(aws --region=ap-southeast-2 --profile=dynamo dynamodb get-item --table-name="smartforms-clusters" --key '{"key": {"S":"/{{ inventory_hostname }}"} }' | jq .Item.value.S | jq  '. | fromjson')

IS_MAINTENANCE=$(echo $DEPLOY_INFO | jq -r .maintenance_mode_enabled)
RELEASE_VER=$(echo $DEPLOY_INFO | jq -r .release)

# First check that maintenance mode is enabled
if [ ! "${IS_MAINTENANCE}" == "true" ] 
then
	echo "Maintenance mode is not enabled, refusing to deploy."
	exit 1
fi

echo "Will deploy ${RELEASE_VER} ..."

# Download the release
rm -f "/root/downloads/smartforms-release-${RELEASE_VER}.zip"
echo "Downloading ${RELEASE_VER} ..."
aws --profile=s3 s3 cp "s3://smartforms-deploy/smartforms-release-${RELEASE_VER}.zip" /root/downloads/
echo "Downloaded."

# Unzip the release
RLS_DIR="/root/downloads/smartforms-release-extracted"
rm -rf "${RLS_DIR}"
unzip -q "/root/downloads/smartforms-release-${RELEASE_VER}.zip" -d "${RLS_DIR}"

# Stop all services
echo "Stopping backend services ..."
systemctl stop backend

# Make sure tomcat is in the grave
echo "Ensuring Tomcat is gone forever ..."
systemctl stop tomcat || true
systemctl disable tomcat || true
systemctl mask tomcat || true

# Take database backup
echo "Taking database backup ..."
mkdir -p /root/database-backups
mysqldump --single-transaction --routines --triggers --all-databases | pigz > /root/database-backups/database-$(date +%Y-%m-%d-%H.%M.%S).sql.gz
echo "Database backup done."

# Move previous installation directories (we will delete them at the end)
if [ -e "/home/backend/jboss" ]
then
	echo "Moving jboss to backup directory"
	mv /home/backend/jboss /home/backend/jboss.bak.$(date +%Y-%m-%d-%H.%M.%S)
fi

# Unzip jboss and copy config files
echo "Extracting JBoss..."
unzip -q "/root/downloads/wildfly.zip" -d /home/backend
mv /home/backend/wildfly-12.0.0.Final /home/backend/jboss
echo "Copying config files for JBoss ..."
\cp ${RLS_DIR}/config-templates/combined/{smartforms.xml,standalone.xml,node.hostname,fop.xml,html-entities.properties} /home/backend/jboss/standalone/configuration/
\cp ${RLS_DIR}/config-templates/combined/standalone.conf /home/backend/jboss/bin/
# Copy principal and credentials into JBoss
SF_PRINCIPAL=$(grep SMARTFORMS_BACKEND_PRINCIPAL /etc/default/backend | cut -d'=' -f 2)
SF_CREDENTIALS=$(grep SMARTFORMS_BACKEND_CREDENTIALS /etc/default/backend | cut -d'=' -f 2)
rm -f /home/backend/jboss/standalone/configuration/{application-users.properties,application-roles.properties}
printf "${SF_PRINCIPAL}=$(echo -n "${SF_PRINCIPAL}:ApplicationRealm:${SF_CREDENTIALS}" | md5sum | awk '{ print $1 }')\n" > /home/backend/jboss/standalone/configuration/application-users.properties
printf "${SF_PRINCIPAL}=cloud-user,smartform-admin,smartform-user\n" > /home/backend/jboss/standalone/configuration/application-roles.properties

# Copy deployments
echo "Copying JBoss modules ..."
unzip -q "${RLS_DIR}/smartform-jboss-modules-0.0.1-SNAPSHOT.jar" -d /home/backend/jboss/modules/
echo "Copying backend ears ..."
\cp "${RLS_DIR}/smartform-enterprise-0.0.1-SNAPSHOT.ear" "/home/backend/jboss/standalone/deployments/127.0.0.1-smartform-enterprise.ear"
\cp "${RLS_DIR}/cloud-manager-0.0.1-SNAPSHOT.ear" "/home/backend/jboss/standalone/deployments/127.0.0.1-cloud-manager.ear"
\cp "${RLS_DIR}/workflow-handler-email-enterprise-0.0.1-SNAPSHOT.ear" "/home/backend/jboss/standalone/deployments/workflow-handler-email-enterprise.ear"
\cp "${RLS_DIR}/smartform-enterprise-ws-0.0.1-SNAPSHOT.ear" "/home/backend/jboss/standalone/deployments/smartform-enterprise-ws.ear"
echo "Copying frontend wars ..."
\cp "${RLS_DIR}/smartform-runtime-portlet-0.0.1-SNAPSHOT.war" "/home/backend/jboss/standalone/deployments/smartform-runtime-portlet.war"
\cp ${RLS_DIR}/smartform-runtime-designer-*.war "/home/backend/jboss/standalone/deployments/smartform-runtime-designer.war"

# Fix permissions
chown -R backend:backend /home/backend/jboss

# Delete old files (but not database backup)
rm -rf /home/backend/jboss.bak.*
rm -rf /home/frontend/tomcat.bak.*
rm -rf "${RLS_DIR}"

# Start everything up again
echo "Starting services (backend) ..."
systemctl start backend

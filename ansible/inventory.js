#!/usr/bin/env node

const AWS = require('aws-sdk');

const credentials = new AWS.SharedIniFileCredentials({ profile: 'smartforms-ansible' });
AWS.config.credentials = credentials

const dynamodb = new AWS.DynamoDB({ region: 'ap-southeast-2' });

var out = {
  'replicas': {
    'hosts': [
      'replica-bl.au.smartfor.ms','replica-sau.au.smartfor.ms', 
      'replica-do.north.smartfor.ms', 'replica-vultr.north.smartfor.ms'
    ]
  }
}

dynamodb.scan({ TableName: 'smartforms-clusters' })
  .promise()
  .then((data) => {
    for (cluster of data.Items) {
      if (cluster.key.S[0] !== '/') {
        continue
      }
      const domain = cluster.key.S.substring(1);
      out['region-' + domain] = { hosts: [ domain ], vars: { domain: domain } };
    }
  })
  .catch((err) => {
    console.log('failed to fetch inventory', err);
    process.exit(1);
  })
  .then(() => {
    console.log(out)
  })

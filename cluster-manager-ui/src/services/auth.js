import axios from 'axios'

const lsKey = 'cmak'

export default {
  authenticated: function () {
    return window.localStorage.getItem(lsKey) !== null
  },
  tryAuthenticate: function (key) {
    return axios({
      method: 'get',
      url: 'https://manager.smartfor.ms/api/v1/clusters',
      headers: {
        'Authorization': key
      }
    }).then(() => {
      window.localStorage.setItem(lsKey, key)
      return Promise.resolve()
    }).catch((err) => {
      return Promise.reject(err)
    })
  },
  logout: function () {
    window.localStorage.removeItem(lsKey)
  }
}

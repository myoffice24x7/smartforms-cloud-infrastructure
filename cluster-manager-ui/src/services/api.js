import axios from 'axios'

const makeRequest = function (method, path, data) {
  return axios({
    method: method || 'get',
    url: 'https://manager.smartfor.ms/api/v1/' + path,
    data: data,
    headers: {
      'Authorization': window.localStorage.getItem('cmak')
    }
  })
}

export default {
  getClusters: function () {
    return makeRequest('get', 'clusters')
  },
  getTags: function () {
    return makeRequest('get', 'tags')
  },
  setCluster: function (name, val) {
    return makeRequest('post', 'cluster/' + name, val)
  },
  getHealth: function (name) {
    return axios({
      method: 'get',
      url: 'https://' + name + '/smartform-runtime-portlet/public/health'
    })
  }
}
